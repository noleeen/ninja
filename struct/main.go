package main

import "fmt"

type Age int //создали тип Age на основе типа int

func (a Age) isAdult() bool { // создали метод булевый для типа Age, который говорит нам совершеннолетний кто-то или нет
	return a >= 18
}

type User struct { // объявили структуру
	name   string
	age    Age // задали тип для этого поля Age
	sex    string
	height int
}

func (u User) printInfo() { // метод структуры (ресивер по ЗНАЧЕНИЮ - создаёт копию и не меняет исходнйы объект структуры)
	fmt.Println(u.name, u.age, u.sex)
}

func (u *User) changeName(name string) { // метод структуры (ресивер по ССЫЛКЕ - работает с местом в памяти поэтому все изменения вносятся в исходный объект)
	u.name = name
}

func NewUser(name, sex string, age, height int) User { // КОНСТРУКТОР
	return User{
		name:   name,
		sex:    sex,
		age:    Age(age), // ПРИВЕДЕНИЕ ТИПОВ (это возможно если типы основаны на одних и тех же типах) переводим int который вводится пользователем в Age который содержит нужный нам метод
		height: height,
	}
}

func main() {
	user1 := User{"Vasya", 23, "male", 177}
	user2 := NewUser("Ivan", "male", 23, 177) // создали новый объект с помощью КОНСТРУКТОРА

	user1.printInfo() //используем метод структуры

	fmt.Printf("%+v\n", user1)
	fmt.Printf("%+v\n", user2)
	fmt.Println(user2.name)
	fmt.Println(user1.name)

	user2.changeName("Vanya") //используем  ещё один метод структуры
	fmt.Printf("%+v\n", user2)

	fmt.Println(user1.age.isAdult()) //  проверяем достиг ли наш юзер совершеннолетия МЕТОДОМ ИЗ ТИПА Age

}
