package main

import (
	"fmt"
	"math"
)

// интерфейс - площадь Area() . ссылаемся на него при создании методов типов квадрат и круг. затем в функции печатьФигурыПлощадь передаём на вход объект с типом интерфейс и выводим результат работы метода типа через интерфейс

type Shape interface {
	Area() float32
}

type Square struct {
	sideLength float32
}

func (s Square) Area() float32 {
	return s.sideLength * s.sideLength
}

type Circle struct {
	radius float32
}

func (c Circle) Area() float32 {
	return c.radius * c.radius * math.Pi
}

func main() {
	s1 := Square{5}
	s2 := Square{7}
	c1 := Circle{3}

	printShaprArea(s1)
	printShaprArea(s2)
	printShaprArea(c1)

	// printInterface(s2)
	// printInterface(22)
	// printInterface('s')
	// printInterface("dss")

	lenString(99)
	lenString("zdfsff")
	lenString('d')
	lenString("d")
}

func printShaprArea(x Shape) {
	fmt.Println(x.Area())
}

// пустой интерфейс который принимает любой объект, проверяет его тип и исходя из этого что-то делает
func printInterface(i interface{}) {
	// fmt.Printf("%+v\n", i)
	switch value := i.(type) { // для t присваиваем тип входящего объекта
	case int:
		fmt.Println("int", value)
	case bool:
		fmt.Println("bool", value)
	case string:
		fmt.Println("string", value)
	default:
		fmt.Println("unknown type", value)
	}
}

//пустой интерфейс для приведения типов: закидываем объект в функцию через пустой интерфейс, если объект соответствует определённому типу, проводим над ним операции, если нет - выводим, что объект не соответствует нужному типу

func lenString(i interface{}) {
	str, ok := i.(string) // переводим входящий объект в строку. параметр ок - булевый!
	if !ok {
		fmt.Println("interface is not string")
	}
	fmt.Println(len(str))

}
