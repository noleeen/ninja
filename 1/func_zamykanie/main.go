package main

import (
	"fmt"
)

func main() {
	i := incr1()
	fmt.Println(i())
	fmt.Println(i())
	i = incr3()
	fmt.Println(i())
	fmt.Println(i())
	fmt.Println()

	fmt.Println()
	fmt.Println(incr2())
	fmt.Println(incr2())
	fmt.Println(incr2())
	a := incr2()
	fmt.Println(a)
	fmt.Println(a)
	fmt.Println(a)

}

func incr1() func() int {
	count := 0
	return func() int {
		count++
		return count
	}
}
func incr3() func() int {
	count := 0
	return func() int {
		count = count + 5
		return count
	}
}

func incr2() int {
	count := 0
	count++
	return count
}
