package main

import "fmt"

// КОМПОЗИЦИЯ ИНТЕРФЕЙСОВ - это когда интерфейс состоит из нескольких интерфейсов
// чтобы объект соответствовал интерфейсу, он должен одновременно имплементировать все методы интерфейса или композиции интерфейсов

type Shape interface { // интерфейс состоит из двух интерфейсов
	ShapeArea
	ShapePerimetr
}

type ShapeArea interface { // интерфейс с методом площадь
	Area() float32
}

type ShapePerimetr interface { // интерфейс с методом периметр
	Perimetr() float32
}

type Square struct {
	sideLength float32
}

func (s Square) Area() float32 { // метод считает площадь
	return s.sideLength * s.sideLength
}

func (s Square) Perimetr() float32 { // метод считает периметр
	return s.sideLength * 4
}

type Circle struct {
	radius float32
}

func (s Circle) Area() float32 { // метод считает площадь
	return s.radius * s.radius * 3.14
}

func (s Circle) Perimetr() float32 { // метод считает периметр
	return s.radius * 2 * 3.14
}

func main() {
	s1 := Square{5}
	s2 := Square{7}
	c1 := Circle{3}
	printInterface(s1)
	printInterface(s2)
	printInterface(c1)
}

func printInterface(i Shape) { // ф-ия принимает объект с типом интерфейса Shape и показывает результат двух методов объекта
	fmt.Println("площадь равна:", i.Area())
	fmt.Println("периметр равен:", i.Perimetr())
}
