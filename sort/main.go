package main

import (
	"fmt"
	"math/rand"
	"sort"
)

type Inter interface{
	Len()int
	Less(i,j int) bool
	Swap(i,j int)
}

type mCard []struct{
	coast int
	name string
}

func (mc mCard) Len()int {
	return len(mc)
}

func (mc mCard) Less(i,j int) bool{
	return mc[i].coast < mc[j].coast
}

func (mc mCard) Swap(i,j int) {
	mc[i], mc[j] = mc[j], mc[i]
}

type sliceInt []int

func (s sliceInt) Len()int {
	return len(s)
}

func (s sliceInt) Less(i,j int) bool{
	return s[i] < s[j]
}

func (s sliceInt) Swap(i,j int) {
	s[i], s[j] = s[j], s[i]
}

func main() {
	mySlice := sliceInt{}
	addNum(&mySlice)
	fmt.Println(mySlice)
	sort.Sort(mySlice) // сортируем тип который имплементирует интерфейсу с методами less len swap
	fmt.Println(mySlice)

	structToSort := mCard{
		{
			coast: 12,
			name: "sdf",
		},
		{
			coast: 1,
			name: "eadf",
		},
		{
			coast: 2,
			name: "arrrsdf",
		},
	}
	sort.Sort(structToSort) // сортируем структуру который имплементирует интерфейсу с методами less len swap
	fmt.Println(structToSort)


}

func addNum(ar *sliceInt)  {
	for i := 0; i < 20; i++ {
		*ar = append(*ar, rand.Intn(100))
	}
}